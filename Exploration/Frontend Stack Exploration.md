# Frontend Stack

## Javascript framework: React
A components-based approach to creating web apps today. The react-dom engine interprets jsx syntax to html-dom elements in runtime (browser) or server side (node).
React is available in several flavours in code-style, with the current version (16.8) favoring usage of functional components with react hooks over class-based components.
State management and control is dealed with in global providers (Context), but can also be engineered with Redux. 
React-codebases rely heavily on a standardized structure when the app grows in functionality, and is best seen as a Ui-framework to present, manipulate and act on user input. React does not enforce major architectural desicions, why it's easy to incorporate as the outermost layer in any type of app, native or web.


## Component structure

```
// Declaring type of props
type TitleProps = { message: string; }; 

// annotate incoming prop types, and return type
export const Title = ({ message }: TitleProps): JSX.Element => <h1>{message}</h1>;


// in use
  <html>
	<Title message="Hello World" />
  </html>

```

Functional components in React/TS will have its properties declared inside the same file. When the codebase grows, more and more types will be needed and repeating is unavoidable. Thus the types can be extracted into separate files, and imported into several components.

```
// ButtonProps.interface.ts

export interface ButtonProps = {
	text?: string;
	btnType?: string;
	btnVariant?: string;
	children?: React.Children;
}


// MyButton.tsx

import ButtonProps from '../interfaces/';

interface MyButtonProps extends ButtonProps {
	icon: React.Jsx.Element
}

const IconButton = ({ 
	text, 
	btnType, 
	btnVariant, 
	children, 
	icon
}: MyButtonProps): Jsx.Element => ( 
	<button ... /> 
);


```


## Context

React Context provides handling of state and passing data to underlying tree of components individually choosing where to consume its current value.

```
import * as React from 'react';

interface AppContextInterface {
	name: string;
	author: string;
	url: string;
}

const AppContext = React.createContext<AppContextInterface | null>(null);

const englishAppContext: AppContextInterface = {
	name: "My Webpage",
	author: "Pablo Anttila",
	url: "pablo.geocities.com"
};

export const App = () => (
	<AppCtx.Provider value={englishAppContext}>
		<NavBar />
		<Page />
		<Footer />
	</AppCtx.Provider>
);

// Footer.tsx 
export const Footer = () => {
	const { name, author } = React.useContext(AppContext);

	return (
		<footer>`${name} - Copyright © {author}`</footer>
	);
};

// NavBar.tsx
export const NavBar = () => {
	const { url } = React.useContext(AppCtx);
	
	return (
		<nav>
			<a href={url}>Back to HomePage</a>
		</nav>
	);
}

```


## Context example with data fetching

```
// continuing on the same file as above, we now split the AppContext into a stateholder object, and an action to set the state with data provided from an apiClient.

interface AppData {
	name: string | null;
	author: string | null;
	url: string | null;
}

interface AppContextInterface extends AppData {
	isLoading: bool;	
}

interface AppActionsContextInterface {
	setData: (request: RequestInfo): Promise;
	setIsLoading: (arg: bool): void;
}

const AppActionsContext = React.createContext<AppActionsContextInterface>();
const AppContext = React.craeteContext<AppContextInterface>();

export const useAppActionsContext = () => useContext(AppActionsContext);
export const useAppContext = () => useContext(AppContext);

const AppContextProvider = (props) => {
	const [isLoading, setIsLoading] = useState(true)

	return (
		<AppContext.Provider value={{isLoading, name: '', author: '', url: ''}}>
			<AppActionsContext.Provider value={{setData, setIsLoading}}>
				{props.children}
			</AppActionsContext.Provider>
		</AppContext.Provider>
	)
}

export default AppContextProvider
```

```
// App.ts

import AppContextProvider from '../context/';

export const App = () => (
	<AppContextProvider>
		<AppWithDataFetch>
			<NavBar />
			<Page />
			<Footer />
		</AppWithDataFetch>
	</AppContextProvider>
);
```

```
// AppWithDataFetch.ts

import { useEffect } from 'react':
import { useAppActionsContext } from '../context';

type AppProps {
	children: React.Children
}
const AppWithDataFetch = props<AppProps> => {
	const { setData, setIsLoading } = useAppActionsContext()
	const { isLoading, ...appData ] = useAppContext()
	useEffect(() => {
		(async () => {
			setIsLoading(true)
			const res = await fetch('/apiDataSource').then((data) => data.json())
			setData(res)
			setIsLoading(false)
		})()
	}, [])

	return (
		<div>
			{!isLoading && children}
		</div>
	)
}

```

This solution will take care of fetching data (once) on initialization of AppWithDataFetch-component, checking for isLoading flag and rendering its children when data has been fetched ans stored in state.


## Ui strategy

My solution will bring primitive ui-components from a standardized open source library (Chakra-Ui) that will be used as building stones to create the theme for our Portal.
This gives us an easy-to-prototype, fast to code and reuse primitives that are themeable and arrangeable to complex components for the interface. Tables, forms, buttons, text elements, eg. Css work is minimized to a fixed set of rules defined in our Theme, that gives us a coherent toolbox for managing sizes, spacing, responsivity, colours and more.

Animations and microanimations like loading, swipes, route-changes are programmatic and controlled by javascript. We will use Framer-motion to achieve a crisp and smooth interface that acts on data and user input.