# Exploration

## Infrastructure
Definera behov för kontinuerlig utveckling i ett 5+ man starkt team. 

  - Azure pipelines; skapa, köra och debugga byggprocesser för CI/CD
  - Azure DevOps vs GitHub; hitta en lösning som kopplar våra Boards till ett lätthanterligt system för pr/ code reviews för befintliga och nya utvecklare
  - Skriva tester som körs automatiskt i byggen och passar i workflow för QA
  - App builds med automatiska previews, miljöer för dev, stage osv
  - Gitflow, code style policies, quality enforcement när flera kodar på frontend

  #### Azure Services:
  Azure erbjuder 3 varianter av Appar som fungerar idag
    - Azure Web Apps (dotnet + react)
    - Azure Static Web Apps (med serverless functions) (available 2021)
    - Azure Blob Storage (statiska filer via cdn)

## Architecture

### React + NodeJs
[Azure Web Apps]
  Traditionellt sätt att köra React-klient med en proxy-backend i nodeJs. All kod i samma repo. Lätt för FE-utvecklare att skapa dev-miljö, inget behov av dotnet. Node hanterar init, routing, serverns funktionalitet begränsas till logik som FE behöver oberoende av vad API matar ut. Tex cache el db/storage som ej hör hemma i API. 
  Tänk: ladda upp grafik, spara bokmärken för användaren, göra dynamiska dashboards per konto, översättningar som bara gäller FE, CMS-lösningar. Dvs saker som skulle kunna bo i klientens local storage.

### dotNet core + React
[Azure Web Apps]
  Bundla React-klienten med data från restful API, server gjort i dotnet. Samma repo. Routing i controllers. Lätt för backend-utvecklare att testa sina nya features med klient i dev-miljö. Byggsteg beskrivs i samma Yaml-fil. All funktionalitet måste byggas i controllers i c#. Klienten exponerar api's http-address. Nyckel ligger synlig i klienten.

Exempel
  - https://dev.to/smithphil/visual-studio-2019-asp-net-core-3-1-react-app-with-typescript-26hi


### React + Azure Functions
[Azure Static Web App]
  - Kan köra funktioner i både js och c#
  - Distribuerar klient via CDN som statiska filer
  - Hög skalbarhet. Startar från 9usd/app

Exempel
  - NestJs (Ts) as Azure functions https://github.com/nestjs/azure-func-http, https://trilon.io/blog/deploy-nestjs-azure-functions
