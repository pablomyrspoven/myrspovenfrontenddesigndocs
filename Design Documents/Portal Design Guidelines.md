# Myrspoven Portal Web App

Author: Pablo Anttila (pablo@myrspoven.se)

Assignee: Pablo Anttila

Created: 2021-09-20

## Problem description
The A-to-B project aims to solve various issues in current interface by updating the UI to a modern standard, separating UX issues from backend and generally aiding the goal to scale gracefully from a business perspective. Stakeholders have a wide range of needs and features to implement, with that in mind, this project will support current functionality and scope a minimum of requirements to bootstrap a barebones project for the dotnet team.

### Background
The Myrspoven Portal acts as the main backoffice for engineers, developers and clients to manage, debug and analyze Myrspoven AI connection to their respective Building Management Systems. It's a secure interface for day to day work for a wide range of applications regarding Myrspovens business. 

### Requirements
Needs from [Team DotNet]
	- CI/CD Pipelines to push, test and deploy production and staging environments
	- Boards-to-repo, workflow that assures planned work gets delivered
	- Testing that respects QA-requirements
	- Gitflow that enforces quality in code in collaborative manner
	
Requirements from [Team Operations]
	- Current workflow presentation, minimum requirements to get work done
	- Features for backlog
	- Problem solving and debugging issues
	
Requirements from [Team Sales]
	- Portal as tool for demonstration
	- Client facing issues, tickets and aids in communication with clients
	
Requirements from [Team AI]
	- What problems can the interface aid you with?
	
Requirements from [Tech Lead & CTO]
	- Implementational details for architecture, infrastructure and future techical debt
	- Onboarding new developers into project
	
	
## Proposed solution
The project aims to create one or several Webapps (SPA), that communicate securely by Oauth2 to a Restful WebApi, thus separating all frontend from backend systems.
The Webapps will be Static Javascript bundles, served from azure app services, Continuous integration by pipelines using Bitbucket, Octopus and Azure.

For the frontend stack:
	- React with Typescript [[Frontend Stack Exploration#Javascript framework React]]
	- State management with React Context [[Frontend Stack Exploration#Context]]
	- Authentication by Oauth2 tokens [[Web Api Swagger]]
	- Ui created with a mix of CSS-in-JS (Emotion), component libraries (Chakra-Ui) and animations in JS (Framer Motion) [[Frontend Stack Exploration#Ui strategy]]
	
	
Tests:
	- Unit Testing with React Testing Library [[Testing Strategy Guidelines]]
	- Visual Testing and design library maintenance with Storybook [[Testing Strategy Guidelines]]
	
