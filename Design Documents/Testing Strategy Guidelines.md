# Testing strategy

## Unit Testing:

### Storybook
Storybook runs alongside your app in development mode. It helps you build UI components isolated from the business logic and context of your app.
> https://storybook.js.org/tutorials/intro-to-storybook/react/en/get-started/

### React Testing Library
[`React Testing Library`](https://github.com/testing-library/react-testing-library) builds on top of `DOM Testing Library` by adding APIs for working with React components.
> https://testing-library.com/docs/react-testing-library/intro/


## Functional Testing:
Proposed industry standard alternatives, as provided by: https://www.browserstack.com/guide/cypress-vs-selenium

### Cypress
Cypress is a purely JavaScript-based front end testing tool built for the modern web. It aims to address the pain points developers or QA engineers face while testing an application.

#### Using Cypress, QAs or developers can create:
1.  Unit tests
2.  Integration tests
3.  End to End tests

#### Limitations of Cypress:
1.  One cannot use Cypress to drive two browsers at the same time
2.  It doesn’t provide support for multi-tabs
3.  Cypress only supports JavaScript for creating test cases
4.  Cypress doesn’t provide support for browsers like Safari and IE at the moment.
5.  Limited support for iFrames

### Selenium
[Selenium](https://www.browserstack.com/selenium "What is Selenium") is a popular test automation tool that automates web-browsers. This open-source tool has been a leading choice for testers for over a decade now.

It allows QAs to automate test cases for the desired browser by using the Selenium WebDriver library along with a language-specific framework

#### Key Advantages of Selenium:
1.  Compatible with multiple OS like Windows, Linux, Unix, Mac
2.  Provides QAs the flexibility to select the programming language of their choice like Java, Ruby, Python, etc.
3.  Compatible with modern browsers like Safari, Chrome, Firefox, etc.
4.  Provides Concise APIs


#### Limitations of Selenium:
1.  No built-in command for automatic generation of test results
2.  Handling page load or element load is difficult
3.  Limited support for testing images
4.  Creating test cases is time-consuming
5.  Difficult to set up test environment as compared to Cypress