## Portal Backoffice


### Sitemap
Startpage
  - Demo / Simulation
  - Docs
  - Release Notes
  - Help / Chat
Login
  - Password recovery
Account
  - Admin
    - List
    - Create / Edit
    - Delete
    - Notify
  - User
    - Profile
    - Edit
    - Invite guest user


#### Logged In (admin):

Blueprints
	- List and Select
	- Upload Blueprints
	- Place Sensors
	- View Sensors
		- Get readout and view reports
	- Connect signals to sensors
		- Map manually
		- Talkpool

Companies
	- List Companies
	- Edit
	- Add/ Remove Buildings
	
Buildings
 	- Building information, write 
	- Messages
	- Processes
	- Add maintenance dates
		
Signals
	- Multiple operations on signals...

Statistics
	- Get signal statistics based off date and building id
	- Show Dataset Viewer
		- Various facets of data
	- Ai data output
	- Charts
	
Processes
	- Get/ switch on processes
	- Process Viewer 
	- Scheduler
	- BMS
	
Message Board
	- View messages related to buildings
	
Webport
	- Create scripts